package com.onetomany.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;

@Entity
@Table(
        name="course",
        uniqueConstraints = {
                @UniqueConstraint(name = "unique_title", columnNames = "title")
        }
)
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;
    @Column(
            name="title",
            nullable = false

    )
    private String title;
    @ManyToOne(fetch=FetchType.LAZY,
            cascade={CascadeType.MERGE,CascadeType.DETACH,
                    CascadeType.PERSIST,CascadeType.REFRESH})
    @JoinColumn(
            name="instructor_id",
            foreignKey = @ForeignKey(name="fk_instructor_id")
    )
    //@JsonIgnore
    private Instructor instructor;

    public Course() {
    }

    public Course(String title, Instructor instructor) {
        this.title = title;
        this.instructor = instructor;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Instructor getInstructor() {
        return instructor;
    }

    public void setInstructor(Instructor instructor) {
        this.instructor = instructor;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", instructor=" + instructor +
                '}';
    }
}
