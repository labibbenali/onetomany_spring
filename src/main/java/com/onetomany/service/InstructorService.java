package com.onetomany.service;

import com.onetomany.model.Instructor;
import com.onetomany.repository.InstructorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InstructorService {

    private final InstructorRepository instructorRepository;
    @Autowired
    public InstructorService(InstructorRepository instructorRepository) {
        this.instructorRepository = instructorRepository;
    }

    public List<Instructor> getInstructors(){
        return this.instructorRepository.findAll();
    }

    public Instructor creteInstructor(Instructor instructor){
        return this.instructorRepository.save(instructor);
    }

    public Instructor getInstructorById(Long id){
        return this.instructorRepository.getById(id);
    }


}
