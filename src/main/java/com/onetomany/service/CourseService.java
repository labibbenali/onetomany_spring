package com.onetomany.service;

import com.onetomany.model.Course;
import com.onetomany.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseService {
private final CourseRepository courseRepository;
    @Autowired
    public CourseService(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    public Course createCourse(Course course){
        return this.courseRepository.save(course);
    }

    public List<Course> getCourses(){
        return this.courseRepository.findAll();
    }

    public void deleteCourse(Course course){
        this.courseRepository.delete(course);
    }

    public Course getCourseById(Long id){
       return this.courseRepository.getById(id);
    }

}
