package com.onetomany.controller;

import com.onetomany.model.Course;
import com.onetomany.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/onetomany")
public class CourseController {
    private final CourseService courseService;

    @Autowired
    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }


    @PostMapping("/courses")
    public Course createCourse(@RequestBody Course course){
       return this.courseService.createCourse(course);
    }

    @GetMapping("/courses")
    public List<Course> getCourses(){
        return this.courseService.getCourses();
    }
    @DeleteMapping("/course/{id}")
    public String deleteCourse(@PathVariable("id") Long id ){
        try {
            Course theCourse = this.courseService.getCourseById(id);
            if (theCourse != null) {
                this.courseService.deleteCourse(theCourse);
                return "Done";
            }
        }catch(Exception e){ return "Id not exist : "+id ;}

        return null;
    }
}
