package com.onetomany.controller;

import com.onetomany.model.Instructor;
import com.onetomany.service.InstructorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/onetomany")
public class InstructorController {
    private final InstructorService instructorService;
    @Autowired
    public InstructorController(InstructorService instructorService) {
        this.instructorService = instructorService;
    }

    @GetMapping("/instructors")
    public List<Instructor> getInstructors(){
        return this.instructorService.getInstructors();
    }

    @PostMapping("/instructors")
    public Instructor creatInstructor(@RequestBody Instructor instructor){
        return this.instructorService.creteInstructor(instructor);
    }

}
