package com.onetomany.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="Instructor")
public class Instructor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(
            name="first_name",
            nullable=false
    )
    private String firstName;
    @Column(
            name="last_name",
            nullable = false
    )
    private String lastName;
    @Column(
            name="email",
            nullable=false
    )
    private String email;
    //Refer to "instructor " property in "Course" class
    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "instructor",
            cascade={CascadeType.DETACH,CascadeType.MERGE,
                    CascadeType.PERSIST, CascadeType.REFRESH }
    )
    private List<Course> courses;

    public Instructor() {
    }


    public List<Course> getCourses() {
       return courses;
   }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    public Instructor(String firstName, String lastName, String email, List<Course> courses) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.courses = courses;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    //add convenience methods for bidirectional relationship

    public void add (Course tempCourse){
        if(courses == null){
            courses=new ArrayList<>();
        }
        courses.add(tempCourse);
        tempCourse.setInstructor(this);
    }

    @Override
    public String toString() {
        return "Instructor{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", courses=" + courses +
                '}';
    }
}
