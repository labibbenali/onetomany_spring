package com.onetomany;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnetomanySpringApplication {
    public static void main(String[] args) {
        SpringApplication.run(OnetomanySpringApplication.class, args);
    }
}
